#Pitt WASP C Code Standard


- [Comments] (#comments)
- [Variables] (#variables)
- [Control Structures] (#control-structures)
- [Whitespace] (#whitespace)

#Comments

Each file shall have an introductory block coment that introduces the file.  This introduction shall include the file name, the author's name, the date the file was last edited, and a brief purpose of the file.

```c
/*
 * FizzBuzz.c
 * Author: John Doe
 *
 * This simple program detemines whether a given number is divible by 3, 5, both, or none
 * If divisible by 3, the program outputs "Fizz".  If divisible by 5, the program outputs "Buzz".
 * If divisible by both 3 and 5, the program outputs "FizzBuzz".
 * If not divisible by 3 or 5 the program outputs an empty string.
*/
```

##Functions

Each fnction shall have a brief introductory comment containing its purpose, arguments, and outputs:

```c
/*
 * takes  given number and determines if it is divisible by 3, 5, both 3 and 5, or neither
 * ...
 * @arg N: number to be analyzed
 * @return string depending on divisibility of given number
 *
/
function fizz_buzz
```

##Explanation

At the author's discretion, single line comments may be inserted above a block of code to help the reader understand its purpose.  Single line comments may also be used to indicate the start or end of specific sections of code.

Block description:
```c
// find the matrix transpose
for (int i = 0; i < N; i++) {

    for (int j = 0; j < N; j++) {
        
        int temp = A[i][j];
        A[i][j] = A[j][i];
        A[j][i] = temp;
    }
}
```

#Variables

##Naming

All variables shall use camelCase with the first letter being lowercase.  Variable names should be as descriptive as possible:

```c
int totalPointsCount;
double subtotalAmount = 5.67;
```

##Initialization

All variables, excluding counting variables used in loops, should be initialized at the top of the scope in which they are needed:

```c
int count;
int startCount;
double theta;

// assignments and actions
...

//counting variable initialized withing loop
for (int i = 0; i < N; i++) {
...
}
```

#Control Structures

- Always surround `if` bodies with curly braces if there is an `else`.  Single-line `if` bodies without an `else` should be on the same line as the `if`.
- All curly braces should begin on the same line as their associated statement.  They should end on a new line. 1TBS
- Put a single space after keywords and before their parentheses.
- No spaces between parentheses and their contents unless it begins with `!`, in which case a space must come before the `!` and after the `!`.

```objective-c
if ( ! goodCondition) return;

if (condition == YES) {
    //do stuff
} else {
    //do other stuff
}
```

#Whitespace

- Indent using 4 spaces.  Never indent with tabs.  Be sure to set this preference in the your IDE.
- Separate imports from the rest of your file by 1 space. Optionally, group imports if there are many (but try to have less dependencies).  Generally strive to include frameworks first.
- When doing logic, a single space should follow the `if` and a single space should preceed the `{`

```objective-c
if (alpha + beta <= 0) && (kappa + phi > 0) {

}
```
